﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace correios
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var ws = new WSCorreios.AtendeClienteClient())
            {
               var result = ws.consultaCEP(txtcep.Text);
                txtendereco.Text = result.end;
                txtcidade.Text = result.cidade;
                txtestado.Text = result.uf;
                txtcomplemento.Text = result.complemento;
            }
        }
    }
}
